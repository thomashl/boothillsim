package controller;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import model.*;

/*
 * This version of the Controller outputs to GUI and takes GUI input.
 */
public class GUIController {
    private int aika = 1440; // Vuoroja; 1 vuoro = ½ minuuttia.
    private Boothill bootti;
    private Pelaaja pelaaja;
    private Map<String, Juoma> juomalista;
    private ArrayList<String> juomanimet;
    private int asiakkaita = 20; // HUOM. jos nimet.json ei sisällä tarpeeksi nimiä, tehdään vähemmän asiakkaita
    private boolean jatka = true;
    private String ilmoitus = "";
    
    public String otaKomento(String arg1, String arg2) throws Exception{
        switch(arg1){
            case "ODOTA": // Passataan vuoro
                break;
            case "JUO": // Juodaan juoma
                if(pelaaja.getJuoma() != null){
                    // On juoma, juodaan se.
                    pelaaja.juoHorppy(pelaaja.getJuoma());
                    break;
                }
                // Ei ole juomaa.
                break;
            case "JUOMA": // Ostetaan juoma
                ostaJuoma(arg2);
                break;
            case "N": case "NE": case "E": case "SE": case "S": case "SW": case "W": case "NW":
                return pelaajaLiiku(arg1);
        }
        return null;
    }
    
    public void ostaJuoma(String juomanimi){
        if(juomanimi == null) return;
        Juoma ur_juoma = juomalista.get(juomanimi);
        Juoma juoma = new Juoma(ur_juoma.getNimi(), ur_juoma.getAlkoholi(), ur_juoma.getTilavuus(), ur_juoma.getHinta());
        // tekstialueelle juttua
        if(pelaaja.getRaha() < juoma.getHinta()){
            // Ei riitä rahat ostaa
            // TODO
        } else {
            // TODO
            pelaaja.setRaha(pelaaja.getRaha() - juoma.getHinta());
            pelaaja.setJuoma(juoma);
        }
    }
    
    public String pelaajaLiiku(String suunta) throws Exception{
        Ruutu ruutu;
        int dx = 0;
        int dy = 0;
        int alkuX = pelaaja.getRuutu().getRuutuX();
        int alkuY = pelaaja.getRuutu().getRuutuY();
        switch (suunta){
            case "N":
                dy = -1;
                break;
            case "NE":
                dx = 1;
                dy = -1;
                break;
            case "E":
                dx = 1;
                break;
            case "SE":
                dx = 1;
                dy = 1;
                break;
            case "S":
                dy = 1;
                break;
            case "SW":
                dx = -1;
                dy = 1;
                break;
            case "W":
                dx = -1;
                break;
            case "NW":
                dx = -1;
                dy = -1;
                break;
        }
        
        ruutu = bootti.getRuutu(alkuX + dx, alkuY + dy);
        
        if(ruutu.isAvoin()){
            if(ruutu.getAsiakas() == null){
                // Ruutu avoin ja tyhjä, liikutan sinne
                pelaaja.setRuutu(ruutu);
            } else {
                // Ruudussa on asiakas, törmätään
                String[] tulos = ruutu.getAsiakas().tormaa(false, pelaaja);
                switch(tulos[0]){
                    case "tappelu-häviö":
                        ilmoitus += tulos[1]+"\n";
                        peliOhi();
                        break;
                    case "tappelu-voitto":
                        ilmoitus += tulos[1]+"\n";
                        break;
                    case "juttelu":
                        ilmoitus += ruutu.getAsiakas().getNimi() + " sanoo: " + tulos[1];
                        break;
                    case "liikkuu":
                        asiakasLiikkuu(ruutu.getAsiakas());
                }
                        
            }
        }
        return ruutu.getTyyppi();
    }
    public String pelaaBiljardi(){
        int tuopinhinta = 420;
                boolean voittiPelin = Biljardi.pelaa(pelaaja);
                if(voittiPelin){
                    pelaaja.setJuoma(new Juoma("kolmosolut", 4.5, 33, tuopinhinta));
                    return "voitto";
                } else {
                    if (pelaaja.getRaha() < tuopinhinta){
                        peliOhi();
                        return "häviöLoppu";
                    } else {
                        pelaaja.setRaha(pelaaja.getRaha()-420);
                        return "häviöRaha";
                    }
                }
    }
    public String pelaaPelikone(){
        boolean voittiPelin = Pelikone.pelaa(pelaaja, 100);
        if(voittiPelin){
            return "voitto";
        } else {
            return "häviö";
        }
        
    }
    public String pelaaTikkataulu(){
        boolean voittiPelin = Darts.pelaa(pelaaja);
        if(voittiPelin){
            return "voitto";
        } else {
            return "häviö";
        }
    }
    public String poistuOvesta(){
        peliOhi();
        return "poistuit";
    }
    public String käyVessassa(){
        // Rakko nollataan kahdesti jottei välissä tapahtuva odotus aiheuta gameoveria.
        if(pelaaja.getJuoma() == null){
            pelaaja.setRakko(0);
            for(int i = 0; i < 5; i++) sykli();
            pelaaja.setRakko(0);
            return "käynti";
        } else {
            return "juoma";
        }
    }
    /*
    public void tarkastaRuutu(Ruutu ruutu) throws Exception{
        // Metodi tarkistaa mitä erikoiskäsittelyjä ruutu aiheuttaa, ja laukaisee ne.
        boolean voittiPelin;
        
        switch(ruutu.getTyyppi()){
            case "baaritiski": // Baaritiski, tarjotaan juomaa ostettavaksi.
                // ostaJuoma();
                if(pelaaja.getJuoma() != null){
                    System.out.println("Sinulla on jo juoma.");
                    break;
                }
                System.out.println("DEBUG: DrinksMenu() laukaustu");
                break;
            case "biljardipöytä":
                int tuopinhinta = 420;
                voittiPelin = Biljardi.pelaa(pelaaja);
                if(voittiPelin){
                    System.out.println("Voitat biliksessä! Häviäjä tarjoaa sinulle oluen.");
                    pelaaja.setJuoma(new Juoma("kolmosolut", 4.5, 33, tuopinhinta));
                } else {
                    if (pelaaja.getRaha() < tuopinhinta){
                        System.out.println("Häviät biliksessä! Koska sinulla ei ole varaa tarjota voittajalle olutta, saat turpaan.");
                        peliOhi();
                    } else {
                        System.out.println("Häviät biliksessä! Joudut tarjoamaan voittajalle oluen.");
                        pelaaja.setRaha(pelaaja.getRaha()-420);
                    }
                }
                break;
            case "pelikone":
                voittiPelin = Pelikone.pelaa(pelaaja, 100);
                if(voittiPelin){
                    System.out.println("Pelaat pelikoneeseen 1€ ... ja voitat!");
                } else {
                    System.out.println("Pelaat pelikoneeseen 1€ ... ja häviät.");
                }
                break;
            case "tikkataulu":
                voittiPelin = Darts.pelaa(pelaaja);
                if(voittiPelin){
                    System.out.println("Pelaat tikkaa ja voitat. Välittääkö kukaan?");
                } else {
                    System.out.println("Pelaat tikkaa ja häviät. Onneksi kukaan ei väliä.");
                }
                break;
            case "ulko-ovi": // Kysytään haluaako poistua, aiheuttaa gameoverin
                peliOhi();
                break;
            case "vessa-ovi": // Käydään vessassa.
                // Rakko nollataan kahdesti jottei välissä tapahtuva odotus aiheuta gameoveria.
                if(pelaaja.getJuoma() == null){
                    System.out.println("Käyt vessassa. Johan helpotti.");
                    pelaaja.setRakko(0);
                    for(int i = 0; i < 5; i++) sykli();
                    pelaaja.setRakko(0);
                } else {
                    System.out.println("Ei juomia vessaan!");
                }
                break;
        }
    }
    */
    
    public ArrayList<String> asiakkaatJuttelee(){
        // Katsotaan onko pelaajan vieressä asiakkaita. Jos on, ne voivat jutustella.
        ArrayList<String> jutut = new ArrayList();
        ArrayList<Ruutu> ruudut = new ArrayList();
        ArrayList<Asiakas> asiakkaat = new ArrayList();
        Ruutu[][] kartta = bootti.getKartta();
        Random rand = new Random();
        int pelaaja_x = pelaaja.getRuutu().getRuutuX();
        int pelaaja_y = pelaaja.getRuutu().getRuutuY();

        for(int x = -1; x < 2; x++){
            for(int y = -1; y < 2; y++){
                if((pelaaja_x + x) < 0 || (pelaaja_y + y) < 0) continue;
                ruudut.add(kartta[pelaaja_y + y][ pelaaja_x + x]);
            }
        }
        for(Ruutu ruutu : ruudut){
            if(ruutu.getAsiakas() != null) asiakkaat.add(ruutu.getAsiakas());
        }
        for(Asiakas asiakas : asiakkaat){
            if(rand.nextInt(10) < 3) jutut.add(asiakas.getNimi() + ": " + asiakas.juttelu());
        }
        return jutut;
    }
    
    public void asiakasLiikkuu(Asiakas asiakas){
        Ruutu ruutu_vanha = asiakas.getRuutu();
        Ruutu ruutu_uusi = asiakas.liiku(bootti);
        if(ruutu_uusi == pelaaja.getRuutu()){
            asiakas.tormaa(true, pelaaja);
        } else if(ruutu_uusi != null) {
            ruutu_vanha.setAsiakas(null);
            ruutu_uusi.setAsiakas(asiakas);
            asiakas.setRuutu(ruutu_uusi);
        }
    }
    
    public void sykli(){
        /* Käydään läpi joka "vuoron" toimenpiteet. Liikutetaan asiakkaita, muutetaan heidän humalatilaa,
         * kutsutaan pelaajan sykli, ja katsostaan onko peli ohi.
         */
        //Ajan kuluminen lisätty, mutta tarkastus puuttuu
        aika--;
        int liikkuuko;
        Random rand = new Random();
        
        for(Asiakas asiakas : bootti.getAsiakkaat()){
            // Käydään läpi asiakkaat. Jokaisen humalatila saattaa vaihtua.
            // Sanity check. Jos asiakas ei ole paikalla, ei ole ruudussa, ja vice versa.
            if(!asiakas.isPaikalla() || asiakas.getRuutu() == null){
                if(asiakas.getRuutu() != null) asiakas.getRuutu().setAsiakas(null);
                asiakas.setRuutu(null);
                continue;
            }

            asiakas.setPromille(asiakas.getPromille() + rand.nextInt(3) - 1);
            
            // Jokainen asiakas saattaa yrittää liikkua.
            liikkuuko = rand.nextInt(4); // Perus mahis liikkua 25% per kierros.
            if(asiakas.getRuutu().getTyyppi().equals("tuoli|tupakka-tuoli")) liikkuuko = rand.nextInt(8); // Tuolissa istujan mahis vain 12.5%.
            
            if(liikkuuko == 0){
                asiakasLiikkuu(asiakas);
            }
        }
        
        // pelaaja.sykli ajaa pelaajan ylläpitotoimet.
        // Jos pelaaja.sykli() palauttaa FALSE, peli on ohi.
        if(pelaaja.sykli()) peliOhi();
    }
    
    public String peliOhi(){
        // Katsotaan miksi peli loppui ja rakennetaan siitä viesti.
        String peli_ohi = "Iltasi Boot Hillissä loppui tähän.\n";
            if(pelaaja.getPromillet() >= 4.0){
                peli_ohi += "\nSinulta meni niinsanotusti 'filmi poikki'.";
                if(pelaaja.getRakko() >= 100) peli_ohi += "\nKun tulet tajuihisi, ihmettelet kuka on pissinyt housuihisi.";
            } else {
                if(pelaaja.getRakko() >= 100) peli_ohi += "\nKastelit housusi. Noloa.";
                if(pelaaja.getHupi() <= 0) peli_ohi += "\nSinulla oli liian mälsää ja lähdit kotiin. Heikkoa.";
            }
        jatka = false;
        ilmoitus += peli_ohi;
        return peli_ohi;
    }
    
    public void init(){
        // Metodi initialisoi pelin alkutilaan.
        
        bootti = new Boothill();
        Ruutu[][] kartta = null;
        ArrayList<Asiakas> asiakkaat = null;
        
        // Juomat listaan
        try {
            juomalista = teeJuomalista();
            juomanimet = new ArrayList();
            for(Map.Entry<String, Juoma> j : juomalista.entrySet()){
                juomanimet.add(j.getKey());
            }
            juomanimet.sort(String::compareToIgnoreCase);
        } catch(IOException e) {
            System.out.println("DEBUG: Virhe IOException listaaJuomat()-metodissa, varmaan huono polku juomat.json-tiedostolla Filereader()-luonnnissa.");
        }
        
        // Luodaan kartta ruutuineen
        try {
            kartta = luoKartta();
        } catch(IOException e) {
            System.out.println("DEBUG: Virhe IOException luoKartta()-metodissa, varmaan huono polku kartta.json-tiedostolla Filereader()-luonnnissa.");
        }
        
        // Luodaan asiakkaat
        try {
            asiakkaat = luoAsiakkaat();
        } catch(IOException e) {
            System.out.println("DEBUG: Virhe IOException luoAsiakas()-metodissa, varmaan huono polku nimet.json tai legend.json-tiedostolla Filereader()-luonnnissa.");
        }
        
        // Luodaan olio jossa on kartta ja asiakkaat.
        if(kartta == null || asiakkaat == null) System.out.println("DEBUG: kartta ja/tai asiakkaat ei luotu.");
        bootti.setKartta(kartta);
        bootti.setAsiakkaat(asiakkaat);
        
        // Luodaan Pelaaja, pohjat 0.05%, alkuraha 50€ (=5000 senttiä), alkuhupi 50, oletuspaino 70kg
        pelaaja = new Pelaaja("Pelaaja", 0.5, 5000, 0, 50, 0, 0, 0, 70, null);
        // String nimi, double promillet, int raha, int pisteet, int hupi, int tupakka, int rakko, int maine, int paino, Juoma juoma
        
        // Pelaaja kartalle, aloituspaikka (2,2)
        pelaaja.setRuutu(bootti.getKartta()[2][2]);
        
        // Asiakkaat ruutuihin
        asiakkaatKartalle();
    }
    
    public Map<String, Juoma> teeJuomalista() throws IOException {
        // Vie juomat.json sisällön listaksi juomaolioita
        Map<String, Juoma> juomat = new HashMap();
        
        JsonReader reader = Json.createReader(new FileReader("src/juomat.json"));
        JsonArray jsona_juomat = reader.readArray();
        
        for(int i = 0; i < jsona_juomat.size(); i++){
            JsonObject obj = jsona_juomat.getJsonObject(i);
            juomat.put(obj.getString("nimi"), new Juoma(obj.getString("nimi"), obj.getJsonNumber("alkoholi").doubleValue(), obj.getInt("tilavuus"), obj.getInt("hinta") )) ;
        }
        
        return juomat;
    }
    
    public Ruutu[][] luoKartta() throws IOException {
        // Luodaan kartta Ruutu-olioista, lukemalla src/kartta.json
        
        Ruutu[][] kartta = new Ruutu[20][22];
        JsonReader reader = Json.createReader(new FileReader("src/kartta.json"));
        JsonArray jsona_ruudut = reader.readArray();
        
        // Luetaan kartta.json Ruutu-array-arrayksi
        for (int y = 0; y < jsona_ruudut.size(); y++){
            for (int x = 0; x < jsona_ruudut.getJsonArray(y).size(); x++){
                JsonObject jsonob = jsona_ruudut.getJsonArray(y).getJsonObject(x);
                kartta[y][x] = new Ruutu(jsonob.getInt("ruutuX"),jsonob.getInt("ruutuY"),jsonob.getString("tyyppi"));
            }
        }
        
        return kartta;
    }
    
    public ArrayList<Asiakas> luoAsiakkaat() throws IOException {
        /* Luodaan asiakkaat (satunnaiset & legendaariset) ja palautetaan.
         * Riippuvaisuudet:
         *  src/nimet.json -- sisältää käytettävät nimet (vähintään yhtä monta kuin ASIAKKAAT-muuttuja)
         *  src/legend.json -- sisältää legendaariset asiakkaat
         *  src/keskustelut.json -- sisältää stringit joita käytetään asiakkaiden jutteluihin
         */
        
        JsonReader nimet_r = Json.createReader(new FileReader("src/nimet.json"));
        JsonArray jsona_nimet = nimet_r.readArray();
        JsonReader keskustelut_r = Json.createReader(new FileReader("src/keskustelut.json"));
        JsonArray jsona_keskustelut = keskustelut_r.readArray();
        JsonReader legend_r = Json.createReader(new FileReader("src/legend.json"));
        JsonArray jsona_legend = legend_r.readArray();
        Random rand = new Random();
        ArrayList<String> nimet = new ArrayList();
        ArrayList<Asiakas> asiakkaat = new ArrayList();
        String[] keskustelut;
        String[] keskustelut_legend;
        String[] kesk_leg_käytä;
        String str_nimi;
        String str_luonne;
        int int_luonne;
        int legendaarisia;
        
        // Luetaan JSON-tiedostosta nimet ArrayListiksi
        for (int i = 0; i < jsona_nimet.size(); i++){
            nimet.add(jsona_nimet.getJsonString(i).toString().replaceAll("\"", ""));
        }
        if(jsona_nimet.size() < asiakkaita) asiakkaita = jsona_nimet.size();
        // Jos nimiä ei ole tarpeeksi, tehdään vähemmän asiakkaita. Sanity check.
        
        // Luetaan JSON-tiedostosta keskustelustringit Arrayksi
        keskustelut = new String[jsona_keskustelut.size()];
        for (int i = 0; i < jsona_keskustelut.size(); i++){
            keskustelut[i] = (jsona_keskustelut.getJsonString(i).toString().replaceAll("\"", ""));
        }
        
        for (int i = 0; i < asiakkaita; i++){
            int_luonne = rand.nextInt(3);
            str_luonne = "aggressiivinen";
            switch (int_luonne){
                case 0:
                    str_luonne = "neutraali";
                    break;
                case 1:
                    str_luonne = "ystävällinen";
            }
            str_nimi = nimet.get(rand.nextInt(nimet.size()));
            nimet.remove(str_nimi);
            
            // String nimi, String luonne, String rooli, boolean paikalla, String[] keskustelut, Pelaaja pelaaja, Boothill bootti
            asiakkaat.add(new Asiakas(str_nimi, str_luonne, "asiakas", true, keskustelut, pelaaja, bootti));
        }
        
        // Lisätään legendaarisia asiakkaita, max. niin monta kuin on, minimi 0
        legendaarisia = rand.nextInt(jsona_legend.size());
        ArrayList<JsonObject> käytössä = new ArrayList();
        for(int i = 0; i < legendaarisia; i++){
            // Otetaan satunnainen legendaarinen asiakas, tarkistetaan ettei ole käytössä, merkitään käytetyksi
            JsonObject obj = jsona_legend.getJsonObject(rand.nextInt(jsona_legend.size()));
            if(käytössä.contains(obj)){
                i--;
                continue;
            }
            käytössä.add(obj);
            
            // Jos legendaarisella asiakkaalla on omat keskustelut, käytetään niitä; jos ei ole, käytetään tavallisia
            keskustelut_legend = null;
            JsonArray jsona_kesk_leg = obj.getJsonArray("keskustelut");
            if (jsona_kesk_leg.size() > 0) keskustelut_legend = new String[jsona_kesk_leg.size()];
            for(int j = 0; j < jsona_kesk_leg.size(); j++){
                keskustelut_legend[j] = jsona_kesk_leg.getString(j);
            }
            if(keskustelut_legend != null){
                kesk_leg_käytä = keskustelut_legend;
            } else {
                kesk_leg_käytä = keskustelut;
            }
            
            asiakkaat.add(new Asiakas(obj.getString("nimi"), obj.getString("luonne"), obj.getString("rooli"), true, kesk_leg_käytä, pelaaja, bootti));
        }
        
        return asiakkaat;
    }
    
    public void asiakkaatKartalle(){
        // Metodi laittaa bootti-muuttujan olioon tallennetut asiakkaat kartalle.
        
        Random rand = new Random();
        ArrayList<Asiakas> asiakkaat = bootti.getAsiakkaat();
        Ruutu[][] kartta = bootti.getKartta();
        int max_y = kartta.length;
        int max_x;
        int y;
        int x;
        
        for (Asiakas asiakas : asiakkaat) {
            do {
                y = rand.nextInt(max_y);
                max_x = kartta[y].length;
                x = rand.nextInt(max_x);
                // Arvotaan uusi paikka kunnes se on avoin ruutu jossa ei ole asiakasta, eikä ole pelaajan aloituspaikka (2,2)
            } while(!kartta[y][x].isAvoin() || kartta[y][x].getAsiakas() != null || (x == 2 && y == 2));
            asiakas.setRuutu(kartta[y][x]);
            kartta[y][x].setAsiakas(asiakas);
        }
        
    }

    public int getAika() {
        return aika;
    }

    public void setAika(int aika) {
        this.aika = aika;
    }

    public Boothill getBootti() {
        return bootti;
    }

    public void setBootti(Boothill bootti) {
        this.bootti = bootti;
    }

    public Pelaaja getPelaaja() {
        return pelaaja;
    }

    public void setPelaaja(Pelaaja pelaaja) {
        this.pelaaja = pelaaja;
    }

    public Map<String, Juoma> getJuomalista() {
        return juomalista;
    }

    public void setJuomalista(Map<String, Juoma> juomalista) {
        this.juomalista = juomalista;
    }

    public int getAsiakkaita() {
        return asiakkaita;
    }

    public void setAsiakkaita(int asiakkaita) {
        this.asiakkaita = asiakkaita;
    }

    public boolean isJatka() {
        return jatka;
    }

    public void setJatka(boolean jatka) {
        this.jatka = jatka;
    }

    public ArrayList<String> getJuomanimet() {
        return juomanimet;
    }

    public String getIlmoitus() {
        return ilmoitus;
    }

    public void setIlmoitus(String ilmoitus) {
        this.ilmoitus = ilmoitus;
    }
    
}