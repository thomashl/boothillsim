package controller;

/*
 * This version of the Controller outputs to console and takes console input.
 */

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import model.*;

public class Controller {
    private static int aika = 1440; // Vuoroja; 1 vuoro = ½ minuuttia.
    private static Boothill bootti;
    private static Pelaaja pelaaja;
    private static ArrayList<Juoma> juomalista;
    private static int asiakkaita = 30; // HUOM. jos nimet.json ei sisällä tarpeeksi nimiä, tehdään vähemmän asiakkaita
    private static boolean jatka = true;
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("TERVETULOA BOOTHILL SIMULAATTORIIN!");
        System.out.println("Numerot 1-9 (numpad + numlock) ja ilmansuunnat n, e, sw, jne. liikkuvat. j juo.");
        init();
        piirto();
//        System.out.print("DEBUG:" bootti.getAsiakkaat().size() +" ");
//        for(Asiakas as : bootti.getAsiakkaat()){
//            System.out.print(as.getNimi()+ " ");
//        }
        do{
            String käsky = sc.next().toLowerCase();
            //System.out.println("Käsky oli: "+käsky);
            if(käsky.matches("n|ne|e|se|s|sw|w|nw")) pelaajaLiiku(käsky);
            if(käsky.matches("[1-4]|[6-9]")){
                int i = Integer.parseInt(käsky);
                String[] stra = new String[]{"sw","s","se","w",null,"e","nw","n","ne"};
                pelaajaLiiku(stra[i-1]);
            }
            if(käsky.matches("j")){
                if(pelaaja.getJuoma() != null){
                    pelaaja.juoHorppy(pelaaja.getJuoma());
                    System.out.println("Juot juomaasi.");
                } else {
                    System.out.println("Ei sinulla ole juomaa! Äkkiä baaritiskille!");
                }
            }
            sykli();
            if(jatka) piirto();
        }while(jatka);
        sc.close();
    }
    
    public static void piirto(){
        Map<String, String> karttasymbolit = new HashMap();
        Ruutu[][] kartta;
        String tekstikartta = "";
        kartta = bootti.getKartta();
        
        // Asiakkaiden juttelu konsoliversiossa on tässä.
        ArrayList<String> jutut = asiakkaatJuttelee();
        if(jutut.size() > 0) for(String juttu : jutut) System.out.println(juttu);
        
        karttasymbolit.put("lattia", ".");
        karttasymbolit.put("seinä", "#");
        karttasymbolit.put("baaritiski", "_");
        karttasymbolit.put("tuoli", "t");
        karttasymbolit.put("pöytä", "T");
        karttasymbolit.put("insinööripöytä", "I");
        karttasymbolit.put("tupakkakoppi", ",");
        karttasymbolit.put("tupakka-tuoli", "t");
        karttasymbolit.put("tupakka-pöytä", "T");
        karttasymbolit.put("ulko-ovi", "U");
        karttasymbolit.put("vessa-ovi", "V");
        karttasymbolit.put("pelikone", "P");
        karttasymbolit.put("biljardipöytä", "B");
        karttasymbolit.put("tikkataulu", "O");
        
        for (Ruutu[] rivi : kartta){
            for (Ruutu ruutu : rivi){
                if(pelaaja.getRuutu() == ruutu) {
                    tekstikartta += "X";
                } else if(ruutu.getAsiakas() != null) {
                    tekstikartta += "!";
                } else {
                    tekstikartta += karttasymbolit.get(ruutu.getTyyppi());
                }
            }
            tekstikartta += "\n";
        }
        System.out.print(tekstikartta);
        String juomana = "ei mitään";
        if(pelaaja.getJuoma() != null) juomana = pelaaja.getJuoma().getNimi();
        int h = aika/120;
        int m = (aika - (h*120)) / 2;
        int s = ((aika - (h*120) - (m*2))%2) * 30;
        System.out.println(pelaaja.getNimi()+": "+pelaaja.getPaino()+"kg "+String.format("%.2f", pelaaja.getPromillet())+"‰ "+(pelaaja.getRaha()/100.00)+"€"+
                "\nHupi: "+pelaaja.getHupi()+" Rakko: "+(pelaaja.getRakko()/2)+"% Pisteet: "+pelaaja.getPisteet()+
                "\n"+String.format("%02d",h)+":"+String.format("%02d",m)+":"+String.format("%02d",s)+" Juomana "+juomana+"");
    }
    
    public static void pelaajaLiiku(String suunta){
        Ruutu ruutu;
        int dx = 0;
        int dy = 0;
        int alkuX = pelaaja.getRuutu().getRuutuX();
        int alkuY = pelaaja.getRuutu().getRuutuY();
        switch (suunta){
            case "n":
                dy = -1;
                break;
            case "ne":
                dx = 1;
                dy = -1;
                break;
            case "e":
                dx = 1;
                break;
            case "se":
                dx = 1;
                dy = 1;
                break;
            case "s":
                dy = 1;
                break;
            case "sw":
                dx = -1;
                dy = 1;
                break;
            case "w":
                dx = -1;
                break;
            case "nw":
                dx = -1;
                dy = -1;
                break;
        }
        
        ruutu = bootti.getRuutu(alkuX + dx, alkuY + dy);
        tarkastaRuutu(ruutu);
        if(ruutu.isAvoin()){
            if(ruutu.getAsiakas() == null){
                // Ruutu avoin ja tyhjä, liikutan sinne
                //DEBUG System.out.println("Liikkui!");
                pelaaja.setRuutu(ruutu);
            } else {
                // Ruudussa on asiakas, törmätään
                //DEBUG System.out.println("Törmäys!");
                String[] tulos = ruutu.getAsiakas().tormaa(false, pelaaja);
                switch(tulos[0]){
                    case "tappelu-häviö":
                        System.out.println(tulos[1]);
                        peliOhi();
                        break;
                    case "tappelu-voitto":
                        System.out.println(tulos[1]);
                        break;
                    case "juttelu":
                        System.out.println(ruutu.getAsiakas().getNimi() + " sanoo: " + tulos[1]);
                        break;
                    case "liikkuu":
                        asiakasLiikkuu(ruutu.getAsiakas());
                }
                        
            }
        }
    }
    
    public static void tarkastaRuutu(Ruutu ruutu){
        // Metodi tarkistaa mitä erikoiskäsittelyjä ruutu aiheuttaa, ja laukaisee ne.
        boolean voittiPelin;
        
        switch(ruutu.getTyyppi()){
            case "baaritiski": // Baaritiski, tarjotaan juomaa ostettavaksi.
                // ostaJuoma();
                if(pelaaja.getJuoma() != null){
                    System.out.println("Sinulla on jo juoma.");
                    break;
                }
                Juoma juoma = juomalista.get(0);
                if(pelaaja.maksa(juoma.getHinta())){
                    System.out.println("Ostettu "+juoma.getNimi());
                    pelaaja.setJuoma(new Juoma(juoma.getNimi(), juoma.getAlkoholi(), juoma.getTilavuus(), juoma.getHinta()));
                } else {
                    System.out.println("Ei ollut varaa ostaa "+juoma.getNimi());
                }
                break;
            case "biljardipöytä":
                int tuopinhinta = 420;
                voittiPelin = Biljardi.pelaa(pelaaja);
                if(voittiPelin){
                    System.out.println("Voitat biliksessä! Häviäjä tarjoaa sinulle oluen.");
                    pelaaja.setJuoma(new Juoma("kolmosolut", 4.5, 33, tuopinhinta));
                } else {
                    if (pelaaja.getRaha() < tuopinhinta){
                        System.out.println("Häviät biliksessä! Koska sinulla ei ole varaa tarjota voittajalle olutta, saat turpaan.");
                        peliOhi();
                    } else {
                        System.out.println("Häviät biliksessä! Joudut tarjoamaan voittajalle oluen.");
                        pelaaja.setRaha(pelaaja.getRaha()-420);
                    }
                }
                break;
            case "pelikone":
                int panos = 100;
                // if(pelaaja.getRaha() < panos) break;
                voittiPelin = Pelikone.pelaa(pelaaja, panos);
                if(voittiPelin){
                    System.out.println("Pelaat pelikoneeseen "+ panos/100.0 +"€ ... ja voitat!");
                } else {
                    System.out.println("Pelaat pelikoneeseen "+ panos/100.0 +"€ ... ja häviät.");
                }
                break;
            case "tikkataulu":
                voittiPelin = Darts.pelaa(pelaaja);
                if(voittiPelin){
                    System.out.println("Pelaat tikkaa ja voitat. Välittääkö kukaan?");
                } else {
                    System.out.println("Pelaat tikkaa ja häviät. Onneksi kukaan ei väliä.");
                }
                break;
            case "ulko-ovi": // Aaiheuttaa gameoverin
                peliOhi();
                break;
            case "vessa-ovi": // Käydään vessassa.
                // Rakko nollataan kahdesti jottei välissä tapahtuva odotus aiheuta gameoveria.
                if(pelaaja.getJuoma() == null){
                    System.out.println("Käyt vessassa. Johan helpotti.");
                    pelaaja.setRakko(0);
                    for(int i = 0; i < 5; i++) sykli();
                    pelaaja.setRakko(0);
                } else {
                    System.out.println("Ei juomia vessaan!");
                }
                break;
        }
    }
    
    public static ArrayList<String> asiakkaatJuttelee(){
        // Katsotaan onko pelaajan vieressä asiakkaita. Jos on, ne voivat jutustella.
        ArrayList<String> jutut = new ArrayList();
        ArrayList<Ruutu> ruudut = new ArrayList();
        ArrayList<Asiakas> asiakkaat = new ArrayList();
        Ruutu[][] kartta = bootti.getKartta();
        Random rand = new Random();
        int pelaaja_x = pelaaja.getRuutu().getRuutuX();
        int pelaaja_y = pelaaja.getRuutu().getRuutuY();

        for(int x = -1; x < 2; x++){
            for(int y = -1; y < 2; y++){
                if((pelaaja_x + x) < 0 || (pelaaja_y + y) < 0) continue;
                ruudut.add(kartta[pelaaja_y + y][ pelaaja_x + x]);
            }
        }
        for(Ruutu ruutu : ruudut){
            if(ruutu.getAsiakas() != null) asiakkaat.add(ruutu.getAsiakas());
        }
        for(Asiakas asiakas : asiakkaat){
            if(rand.nextInt(10) < 3) jutut.add(asiakas.getNimi() + ": " + asiakas.juttelu());
        }
        return jutut;
    }
    
    public static void asiakasLiikkuu(Asiakas asiakas){
        Ruutu ruutu_vanha = asiakas.getRuutu();
        Ruutu ruutu_uusi = asiakas.liiku(bootti);
        if(ruutu_uusi == pelaaja.getRuutu()){
            asiakas.tormaa(true, pelaaja);
        } else if(ruutu_uusi != null) {
            ruutu_vanha.setAsiakas(null);
            ruutu_uusi.setAsiakas(asiakas);
            asiakas.setRuutu(ruutu_uusi);
        }
    }
    
    public static void sykli(){
        /* Käydään läpi joka "vuoron" toimenpiteet. Liikutetaan asiakkaita, muutetaan heidän humalatilaa,
         * kutsutaan pelaajan sykli, ja katsostaan onko peli ohi.
         */
        int liikkuuko;
        Random rand = new Random();
        
        for(Asiakas asiakas : bootti.getAsiakkaat()){
            // Käydään läpi asiakkaat. Jokaisen humalatila saattaa vaihtua.
            // Sanity check. Jos asiakas ei ole paikalla, ei ole ruudussa, ja vice versa.
            if(!asiakas.isPaikalla() || asiakas.getRuutu() == null){
                if(asiakas.getRuutu() != null) asiakas.getRuutu().setAsiakas(null);
                asiakas.setRuutu(null);
                continue;
            }

            asiakas.setPromille(asiakas.getPromille() + rand.nextInt(3) - 1);
            
            // Jokainen asiakas saattaa yrittää liikkua.
            liikkuuko = rand.nextInt(4); // Perus mahis liikkua 25% per kierros.
            if(asiakas.getRuutu().getTyyppi().equals("tuoli|tupakka-tuoli")) liikkuuko = rand.nextInt(8); // Tuolissa istujan mahis vain 12.5%.
            
            if(liikkuuko == 0){
                asiakasLiikkuu(asiakas);
            }
        }
        
        aika += 1;
        if(aika >= 2160) peliOhi();
        
        // pelaaja.sykli ajaa pelaajan ylläpitotoimet.
        // Jos pelaaja.sykli() palauttaa FALSE, peli on ohi.
        if(pelaaja.sykli()) peliOhi();
    }
    
    public static String peliOhi(){
        // Katsotaan miksi peli loppui ja rakennetaan siitä viesti.
        String peli_ohi = "Iltasi Boothillissä loppui tähän.\n";
            if(pelaaja.getPromillet() >= 4.0){
                peli_ohi += "\nSinulta meni niinsanotusti 'filmi poikki'.";
                if(pelaaja.getRakko() >= 100) peli_ohi += "\nKun tulet tajuihisi, ihmettelet kuka on pissinyt housuihisi.";
            } else {
                if(pelaaja.getRakko() >= 100) peli_ohi += "\nKastelit housusi. Noloa.";
                if(pelaaja.getHupi() <= 0) peli_ohi += "\nSinulla oli liian mälsää ja lähdit kotiin. Heikkoa.";
            }
        jatka = false;
        System.out.println(peli_ohi);
        return peli_ohi;
    }
    
    public static void init(){
        // Metodi initialisoi pelin alkutilaan.
        
        bootti = new Boothill();
        Ruutu[][] kartta = null;
        ArrayList<Asiakas> asiakkaat = null;
        
        // Juomat listaan
        try {
            juomalista = teeJuomalista();
            ArrayList<String> juomanimet = new ArrayList();
            juomalista.forEach((j) -> {
                juomanimet.add(j.getNimi());
                juomanimet.sort(String::compareToIgnoreCase);
            });
        } catch(IOException e) {
            System.out.println("DEBUG: Virhe IOException listaaJuomat()-metodissa, varmaan huono polku juomat.json-tiedostolla Filereader()-luonnnissa.");
        }
        
        // Luodaan kartta ruutuineen
        try {
            kartta = luoKartta();
        } catch(IOException e) {
            System.out.println("DEBUG: Virhe IOException luoKartta()-metodissa, varmaan huono polku kartta.json-tiedostolla Filereader()-luonnnissa.");
        }
        
        // Luodaan asiakkaat
        try {
            asiakkaat = luoAsiakkaat();
        } catch(IOException e) {
            System.out.println("DEBUG: Virhe IOException luoAsiakas()-metodissa, varmaan huono polku nimet.json tai legend.json-tiedostolla Filereader()-luonnnissa.");
        }
        
        // Luodaan olio jossa on kartta ja asiakkaat.
        if(kartta == null || asiakkaat == null) System.out.println("DEBUG: kartta ja/tai asiakkaat ei luotu.");
        bootti.setKartta(kartta);
        bootti.setAsiakkaat(asiakkaat);
        
        // Luodaan Pelaaja, pohjat 0.05%, alkuraha 50€ (=5000 senttiä), alkuhupi 50, oletuspaino 70kg
        pelaaja = new Pelaaja("Pelaaja", 0.5, 5000, 0, 50, 0, 0, 0, 70, null);
        // String nimi, double promillet, int raha, int pisteet, int hupi, int tupakka, int rakko, int maine, int paino, Juoma juoma
        
        // Pelaaja kartalle, aloituspaikka (2,2)
        pelaaja.setRuutu(bootti.getKartta()[2][2]);
        
        // Asiakkaat ruutuihin
        asiakkaatKartalle();
    }
    
    public static ArrayList<Juoma> teeJuomalista() throws IOException {
        // Vie juomat.json sisällön listaksi juomaolioita
        
        ArrayList<Juoma> lista = new ArrayList();
        JsonReader reader = Json.createReader(new FileReader("src/juomat.json"));
        JsonArray jsona_juomat = reader.readArray();
        
        for(int i = 0; i < jsona_juomat.size(); i++){
            JsonObject obj = jsona_juomat.getJsonObject(i);
            lista.add(new Juoma(obj.getString("nimi"), obj.getJsonNumber("alkoholi").doubleValue(), obj.getInt("tilavuus"), obj.getInt("hinta") ));
        }
        
        return lista;
    }
    
    public static Ruutu[][] luoKartta() throws IOException {
        // Luodaan kartta Ruutu-olioista, lukemalla src/kartta.json
        
        Ruutu[][] kartta = new Ruutu[20][22];
        JsonReader reader = Json.createReader(new FileReader("src/kartta.json"));
        JsonArray jsona_ruudut = reader.readArray();
        
        // Luetaan kartta.json Ruutu-array-arrayksi
        for (int y = 0; y < jsona_ruudut.size(); y++){
            for (int x = 0; x < jsona_ruudut.getJsonArray(y).size(); x++){
                JsonObject jsonob = jsona_ruudut.getJsonArray(y).getJsonObject(x);
                kartta[y][x] = new Ruutu(jsonob.getInt("ruutuX"),jsonob.getInt("ruutuY"),jsonob.getString("tyyppi"));
            }
        }
        
        return kartta;
    }
    
    public static ArrayList<Asiakas> luoAsiakkaat() throws IOException {
        /* Luodaan asiakkaat (satunnaiset & legendaariset) ja palautetaan.
         * Riippuvaisuudet:
         *  src/nimet.json -- sisältää käytettävät nimet (vähintään yhtä monta kuin ASIAKKAAT-muuttuja)
         *  src/legend.json -- sisältää legendaariset asiakkaat
         *  src/keskustelut.json -- sisältää stringit joita käytetään asiakkaiden jutteluihin
         */
        
        JsonReader nimet_r = Json.createReader(new FileReader("src/nimet.json"));
        JsonArray jsona_nimet = nimet_r.readArray();
        JsonReader keskustelut_r = Json.createReader(new FileReader("src/keskustelut.json"));
        JsonArray jsona_keskustelut = keskustelut_r.readArray();
        JsonReader legend_r = Json.createReader(new FileReader("src/legend.json"));
        JsonArray jsona_legend = legend_r.readArray();
        Random rand = new Random();
        ArrayList<String> nimet = new ArrayList();
        ArrayList<Asiakas> asiakkaat = new ArrayList();
         String[] keskustelut;
        String[] keskustelut_legend;
        String[] kesk_leg_käytä;
        String str_nimi;
        String str_luonne;
        int int_luonne;
        int legendaarisia;
        
        // Luetaan JSON-tiedostosta nimet ArrayListiksi
        for (int i = 0; i < jsona_nimet.size(); i++){
            nimet.add(jsona_nimet.getJsonString(i).toString().replaceAll("\"", ""));
        }
        if(jsona_nimet.size() < asiakkaita) asiakkaita = jsona_nimet.size();
        // Jos nimiä ei ole tarpeeksi, tehdään vähemmän asiakkaita. Sanity check.
        
        // Luetaan JSON-tiedostosta keskustelustringit Arrayksi
        keskustelut = new String[jsona_keskustelut.size()];
        for (int i = 0; i < jsona_keskustelut.size(); i++){
            keskustelut[i] = (jsona_keskustelut.getJsonString(i).toString().replaceAll("\"", ""));
        }
        
        for (int i = 0; i < asiakkaita; i++){
            int_luonne = rand.nextInt(3);
            str_luonne = "aggressiivinen";
            switch (int_luonne){
                case 0:
                    str_luonne = "neutraali";
                    break;
                case 1:
                    str_luonne = "ystävällinen";
            }
            str_nimi = nimet.get(rand.nextInt(nimet.size()));
            nimet.remove(str_nimi);
            
            // String nimi, String luonne, String rooli, boolean paikalla, String[] keskustelut, Pelaaja pelaaja, Boothill bootti
            asiakkaat.add(new Asiakas(str_nimi, str_luonne, "asiakas", true, keskustelut, pelaaja, bootti));
        }
        
        // Lisätään legendaarisia asiakkaita, max. niin monta kuin on, minimi 0
        legendaarisia = rand.nextInt(jsona_legend.size());
        ArrayList<JsonObject> käytössä = new ArrayList();
        for(int i = 0; i < legendaarisia; i++){
            // Otetaan satunnainen legendaarinen asiakas, tarkistetaan ettei ole käytössä, merkitään käytetyksi
            JsonObject obj = jsona_legend.getJsonObject(rand.nextInt(jsona_legend.size()));
            if(käytössä.contains(obj)){
                i--;
                continue;
            }
            käytössä.add(obj);
            
            // Jos legendaarisella asiakkaalla on omat keskustelut, käytetään niitä; jos ei ole, käytetään tavallisia
            keskustelut_legend = null;
            JsonArray jsona_kesk_leg = obj.getJsonArray("keskustelut");
            if (jsona_kesk_leg.size() > 0) keskustelut_legend = new String[jsona_kesk_leg.size()];
            for(int j = 0; j < jsona_kesk_leg.size(); j++){
                keskustelut_legend[j] = jsona_kesk_leg.getString(j);
            }
            if(keskustelut_legend != null){
                kesk_leg_käytä = keskustelut_legend;
            } else {
                kesk_leg_käytä = keskustelut;
            }
            
            asiakkaat.add(new Asiakas(obj.getString("nimi"), obj.getString("luonne"), obj.getString("rooli"), true, kesk_leg_käytä, pelaaja, bootti));
        }
        
        return asiakkaat;
    }
    
    public static void asiakkaatKartalle(){
        // Metodi laittaa bootti-muuttujan olioon tallennetut asiakkaat kartalle.
        
        Random rand = new Random();
        ArrayList<Asiakas> asiakkaat = bootti.getAsiakkaat();
        Ruutu[][] kartta = bootti.getKartta();
        int max_y = kartta.length;
        int max_x;
        int y;
        int x;
        
        for (Asiakas asiakas : asiakkaat) {
            do {
                y = rand.nextInt(max_y);
                max_x = kartta[y].length;
                x = rand.nextInt(max_x);
                // Arvotaan uusi paikka kunnes se on avoin ruutu jossa ei ole asiakasta, eikä ole pelaajan aloituspaikka (2,2)
            } while(!kartta[y][x].isAvoin() || kartta[y][x].getAsiakas() != null || (x == 2 && y == 2));
            asiakas.setRuutu(kartta[y][x]);
            kartta[y][x].setAsiakas(asiakas);
        }
        
    }
    
}