/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.GUIController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.util.Random;


/**
 *
 * @author Francesco
 */
public class BootHillSim extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        FXMLDocumentController.initController();
        primaryStage(stage);
    }
    public void primaryStage(Stage stage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setTitle(randomTitle());
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    public String randomTitle(){
        Random r = new Random();
        String[] titles = {"Rum Roman and the temple of Das Boot","There will be alcohol... and blood","BoothillSim 2: Drunkception - coming soon!","Boothill Simulator, for when the real deal hasn't given you enough cancer"};
        int i = r.nextInt(20);
        //System.out.println(i);
        if(i<18){
            return "Boothill Simulator";
        }else{
            return titles[r.nextInt(titles.length-1)];
        }
    }
    
}
