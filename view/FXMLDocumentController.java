/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.GUIController;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import model.*;

/**
 *
 * @author Francesco
 */
public class FXMLDocumentController implements Initializable {
    
    
    @FXML
    public static ChoiceBox cb = new ChoiceBox();
    @FXML
    public GridPane alusta = new GridPane();
    @FXML
    public Label labelRaha = new Label();
    @FXML
    public Label labelPisteet = new Label();
    @FXML
    public Label labelPromille = new Label(); 
    @FXML
    public Label labelRakko;// = new Label();
    @FXML
    public Label labelHupi = new Label();
    @FXML
    public Label labelJuoma = new Label();
    @FXML
    public Label labelTeksti = new Label();
    @FXML
    public Label labelAika = new Label();
    @FXML
    public Stage stageDrinks = new Stage();
    @FXML
    public static int chosen;
    @FXML
    public static GUIController controller;
    @FXML
    private static String chosenDrink;
    
    
    @FXML 
    public void keyPressed(KeyEvent key) throws Exception{
        String direction = "ODOTA";
        switch(key.getCode()){
            case W: case NUMPAD8: case UP:
                direction = "N";
                break;
            case E: case NUMPAD9:
                direction = "NE";
                break;
            case D: case NUMPAD6: case RIGHT:
                direction = "E";
                break;
            case C: case NUMPAD3:
                direction = "SE";
                break;
            case X: case NUMPAD2: case DOWN:
                direction = "S";
                break;
            case Z: case NUMPAD1:
                direction = "SW";
                break;
            case A: case NUMPAD4: case LEFT:
                direction = "W";
                break;
            case Q: case NUMPAD7:
                direction = "NW";
                break;
            case S: case PERIOD: case NUMPAD5:
                direction = "ODOTA";
                break;
            default:
                break;
        }
        toController(direction,null);
        
    }
    @FXML
    public void juo() throws Exception{
        toController("JUO",null);
    }
    public void toController(String arg1, String arg2) throws Exception{
        if(!controller.isJatka()){
            return;
        }
        String tyyppi = controller.otaKomento(arg1, arg2);
        
        if(tyyppi!=null){
        switch(tyyppi){
            case "baaritiski":
                if(controller.getPelaaja().getJuoma()==null){
                    drinksMenu();
                }else{
                    controller.setIlmoitus(controller.getIlmoitus()+" Sinulla on jo juoma.");
                }
                break;
            case "biljardipöytä":
                String biljardiTulos = controller.pelaaBiljardi();
                switch (biljardiTulos) {
                    case "voitto":
                        controller.setIlmoitus(controller.getIlmoitus()+" Voitit biljardissa oluen.");
                        break;
                    case "häviöLoppu":
                        controller.setIlmoitus(controller.getIlmoitus()+" Hävisit ja sinulla ei ole varaa maksaa olutta, joten sait turpaan.");
                        labelTeksti.setText("Hävisit ja sinulla ei ole varaa maksaa olutta, joten sait turpaan.");

                        break;
                    case "häviöRaha":
                        controller.setIlmoitus(controller.getIlmoitus()+" Hävisit ja jouduit maksamaan häviöstäsi oluen.");
                        break;
                    default:
                        System.out.println("ERROR IN BILJARDI");
                }
                break;
            case "pelikone":
                String pelikoneTulos = controller.pelaaPelikone();
                switch(pelikoneTulos){
                    case "voitto":
                        controller.setIlmoitus(controller.getIlmoitus()+" Pelaat pelikoneeseen 1€ ... ja voitat!");
                        break;
                    case "häviö":
                        controller.setIlmoitus(controller.getIlmoitus()+" Pelaat pelikoneeseen 1€ ... ja häviät.");
                    break;
                }
                break;
            case "tikkataulu":
                String tikkatauluTulos = controller.pelaaTikkataulu();
                switch(tikkatauluTulos){
                    case "voitto":
                        controller.setIlmoitus(controller.getIlmoitus()+" Pelaat tikkaa ja voitat. Välittääkö kukaan?");
                        break;
                    case "häviö":
                        controller.setIlmoitus(controller.getIlmoitus()+" Pelaat tikkaa ja häviät. Onneksi kukaan ei välitä.");
                        break;
                }
                break;
            case "ulko-ovi":
                String poistuminen = controller.poistuOvesta();
                switch(poistuminen){
                    case "poistuit":
                        controller.setIlmoitus(controller.getIlmoitus()+" Poistuit BootHillistä. Häpeä itseäsi.");
                        labelTeksti.setText("Poistuit BootHillistä. Häpeä itseäsi.");
                }
                break;
            case "vessa-ovi":
                String vessaKäynti = controller.käyVessassa();
                switch(vessaKäynti){
                    case "käynti":
                        controller.setIlmoitus(controller.getIlmoitus()+" Käyt vessassa. Johan helpotti.");
                        break;
                    case "juoma":
                        controller.setIlmoitus(controller.getIlmoitus()+" Ei juomia vessaan!");
                        break;
                }
               
            }
        }
        controller.sykli();
        mapUpdate();
    }
    @FXML
    public void mapUpdate(){
        labelRaha.setText("Raha: "+controller.getPelaaja().getRaha()/100.0);
        if(controller.getPelaaja().getJuoma()!=null){
            labelJuoma.setText("Juoma:" +controller.getPelaaja().getJuoma().getNimi());
        }else{
            labelJuoma.setText("Ei ole juomaa");
            
        }
        labelPromille.setText("Promillet: "+String.format("%.2f",controller.getPelaaja().getPromillet())+"‰ ");
        labelRakko.setText("Rakko: "+controller.getPelaaja().getRakko()/2.0+"%");
        labelHupi.setText("Hupi: "+controller.getPelaaja().getHupi()+"%");
        labelPisteet.setText("Pisteet: "+controller.getPelaaja().getPisteet());
        labelTeksti.setText(controller.getIlmoitus());
        int aika = 1440-controller.getAika();
        if(aika%2!=0){
            aika++;
        }
        int min = (aika/2)%60;
        int h = (aika-min)/60;
        if(min<10){
            labelAika.setText(""+(12+h)+":0"+min);
        }else{
            labelAika.setText(""+(12+h)+":"+min);
        }
        
        controller.setIlmoitus("");
        Ruutu[][] kartta = controller.getBootti().getKartta();
        alusta.getChildren().clear();
        for (Ruutu[] rivi : kartta){
            for (Ruutu ruutu : rivi){
                if(ruutu.getAsiakas() != null){
                    //alusta.getChildren().add(new ImageView(new Image("/other.png",20,20,true,false)));
                    alusta.add(new ImageView(new Image("/other.png",20,20,true,false)), ruutu.getRuutuX(), ruutu.getRuutuY());
                }
            }
        }
        ImageView pelaajaView = new ImageView(new Image("/player.png",20,20,true,false));
        alusta.add(pelaajaView, controller.getPelaaja().getRuutu().getRuutuX(), controller.getPelaaja().getRuutu().getRuutuY());
        alusta.toFront();
        
        
        
    }
        /*alusta.getChildren().add(miesView);
        ImageView miesView = new ImageView(new Image("/RedDude.png",20,20,true,false));
        alusta.getChildren().clear();
        alusta.add(miesView, x, y);
        miesView.toFront();*/
    
    
    @FXML
    public void drinksMenu() throws Exception{
        Stage stage = new Stage();

        ChoiceBox choiceBox = new ChoiceBox();
        choiceBox.setPrefWidth(100);
        for(String nimi:controller.getJuomanimet()){
            choiceBox.getItems().add(nimi);
        }
        
        
        Button ostaButton = new Button();
        ostaButton.setText("Osta");
        
        ostaButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e){
                chosenDrink = choiceBox.getItems().get(choiceBox.getSelectionModel().getSelectedIndex()).toString();
                stage.close();
                }
        });
        
        
        
        Label labelDrink = new Label();
        labelDrink.setText("Osta juoma");
        

        AnchorPane a = new AnchorPane(choiceBox, ostaButton, labelDrink);
        choiceBox.setLayoutX(50);
        choiceBox.setLayoutY(50);
        ostaButton.setLayoutX(150);
        ostaButton.setLayoutY(50);
        labelDrink.setLayoutX(50);
        labelDrink.setLayoutY(20);
        Scene scene = new Scene(a, 200, 100);
        stage.setScene(scene);
        stage.showAndWait();
        toController("JUOMA",chosenDrink);
    }
    @FXML
    public static void initController(){
        controller = new GUIController();
        controller.init();
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mapUpdate();
    }
    
}


