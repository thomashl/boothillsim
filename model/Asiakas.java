package model;
import java.util.Random;

public class Asiakas {
    private String nimi;
    private int promille; // Välillä 0-3
    private String luonne;
    private String rooli;
    private int vanhaX = 0; // Edellisen liikkeen suunta, ei koordinaatti
    private int vanhaY = 0; // Edellisen liikkeen suunta, ei koordinaatti
    private boolean paikalla;
    private Ruutu ruutu;
    private Boothill bootti;
    private String[] keskustelut;

    public Asiakas(String nimi, String luonne, String rooli, boolean paikalla, String[] keskustelut, Pelaaja pelaaja, Boothill bootti) {
        Random r = new Random();
        this.nimi = nimi;
        this.luonne = luonne;
        this.rooli = rooli;
        this.promille = r.nextInt(4);
        this.paikalla = paikalla;
        this.keskustelut = keskustelut;
        this.bootti = bootti;
    }

    public String[] tormaa(boolean asiakasTormasi, Pelaaja pelaaja){
        String retstra[] = new String[2];
        double tappeluTod = 1.0;
        double jutteluTod = 1.0;
        double tyhjaTod = 1.0;
        double liikkuuTod = 1.0;
        //Luonnetyypit: aggressiivinen, neutraali, ystävällinen
        switch(this.luonne){
            case "aggressiivinen":
                tappeluTod = tappeluTod * this.promille;
                if(tappeluTod < 1.0) tappeluTod = 1.0;
                break;
            case "neutraali":
                liikkuuTod = liikkuuTod * this.promille;
                if(liikkuuTod < 1.0) liikkuuTod = 1.0;
                break;
            case "ystävällinen":
                jutteluTod = jutteluTod * this.promille;
                tappeluTod = 0;
                if(jutteluTod < 1.0) jutteluTod = 1.0;
        }

        if(asiakasTormasi){
            liikkuuTod = 0;
            tappeluTod -= 1.0;
        }
        
        double yht = tappeluTod + jutteluTod + tyhjaTod + liikkuuTod;
        double rHit = Math.random() + 0.001 * yht;
        // Math.random() on inclusive of 0.0, joten pakko lisätä 0.001 jotta ystävälliset eivät tappele
        if(rHit<tappeluTod){
            if (tappelu(pelaaja)){
                retstra[0] = "tappelu-voitto";
                retstra[1] = this.nimi+" haastaa riitaa. Vedät sitä turpaan.";
            } else {
                retstra[0] = "tappelu-häviö";
                retstra[1] =  this.nimi+" vetää sua turpaan koska luuli että haastoit riitaa.";
            }
        }else if(rHit<jutteluTod+tappeluTod){
            retstra[0] = "juttelu";
            retstra[1] = juttelu();
        }else if(rHit<tyhjaTod+jutteluTod+tappeluTod){
            retstra[0] = "tyhjä";
            retstra[1] = "tyhjä";
        }else{
            retstra[0] = "liikkuu";
            retstra[1] = "liikkuu";
        }
        return retstra;
    }
    
    public String juttelu(){
        Random r = new Random();
        String keskustelu = this.keskustelut[r.nextInt(this.keskustelut.length)];
        return keskustelu;
    }
    public boolean tappelu(Pelaaja pelaaja) {
        // return true; jos pelaaja voittaa, muuten return false
        int voittaakoPelaaja = 50;
            double pelaajaPromillet = pelaaja.getPromillet();
            int pelaajaPaino = pelaaja.getPaino(); // haetaan pelaajan tiedot
                
            double asiakasPromillet = this.promille; // haetaan asiakkaan tiedot
                
            if (this.rooli == null) { //Tarkistetaan onko kyseessä tavallinen asiakas
                if (pelaajaPromillet < asiakasPromillet) {      //Lasketaan todennäköisyys pelaajan voittamiselle %
                    voittaakoPelaaja = voittaakoPelaaja + 25;
                }else if (pelaajaPromillet > asiakasPromillet) {
                    voittaakoPelaaja = voittaakoPelaaja - 25;
                }
                if (pelaajaPaino >= 80 && pelaajaPaino <= 100) {
                    voittaakoPelaaja = voittaakoPelaaja + 25;
                }else {
                    voittaakoPelaaja = voittaakoPelaaja - 25;
                }
                    
                    
            }else {     //tänne vain jos asiakkaalla on jokin erityinen rooli. Näillä henkilöillä on aina sama mahdollisuus voittaa
                switch (this.rooli) {
                    case "Rommi Roman":
                        voittaakoPelaaja = 0;
                        break;
                    case "Joulupukki":
                        voittaakoPelaaja = 5;
                        break;
                    case "Stetsoni":
                        voittaakoPelaaja = 10;
                        break;
                    }
                }        
        double voittaakoAsiakas = (Math.random()) * 100;       //tässä kohtaa tapellaan
        if (voittaakoPelaaja >= voittaakoAsiakas) {
            pelaaja.setMaine(pelaaja.getMaine() + 50);
            pelaaja.setPisteet(pelaaja.getPisteet() + 50);
            this.paikalla = false;      //asiakas poistuu
            return true;
        }else {
            return false;
        }
                    
                
    }
    
    public Ruutu liiku(Boothill bootti) {
        // Asiakas koittaa liikkua satunnaiseen suuntaan.
        // Jos valittu ruutu on avoin ja siinä ei ole asiakasta, palautetaan se.
        // (Kutsuja tarkistaa onko ruudussa pelaaja.)
        Ruutu kohderuutu;
        int x = ruutu.getRuutuX();
        int y = ruutu.getRuutuY();
        int dx;
        int dy;
        int yrityksiä = 10; // Kuinka monta kertaa koitetaan liikkua.
        Random rand = new Random();
        // Asiakkailla on preferenssi olla liikkumatta taaksepäin.
        // Jos (dx * vanhaX + dy * vanhaY < 0) liike olisi taaksepäin.
        
        // Koitetaan liikkua [yrityksiä] kertaa, jos ei onnistu niin pysytään paikalla.
        if(ruutu.getTyyppi().equals("insinööripöytä")) yrityksiä += 20;
        for (int i = 0; i < yrityksiä; i++){
            dx = rand.nextInt(3) - 1;
            dy = rand.nextInt(3) - 1;
            
            // 1/2 mahis hylätä suunta jos se olisi taaksepäin tai vinosti taaksepäin.
            if((dx * vanhaX + dy * vanhaY) < 0 && rand.nextInt(2) == 0) continue;
            // Ei koiteta mennä ulos kartalta.
            if((y+dy) < 0 || (x+dx) < 0) continue;
            if((y+dy) >= bootti.getKartta().length || (x+dx) >= bootti.getKartta()[0].length ) continue;
            kohderuutu = bootti.getKartta()[y+dy][x+dx];
            
            // Onko suunnaksi valittu ruutu avoin ja tyhjä?
            if( (!kohderuutu.isAvoin() && !rooli.equals("Rommi Roman")) || kohderuutu.getAsiakas() != null) continue;
            
            // Ruutu oli ok, lähetetään se valittuna kohteena takaisin.
            return kohderuutu;
        }
        return null;
    }

    public String getNimi() {
        return nimi;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    public int getPromille() {
        return promille;
    }

    public void setPromille(int promille) {
        this.promille = promille;
        if(this.promille > 3) this.promille = 3;
        if(this.promille < 0) this.promille = 0;
    }

    public String getLuonne() {
        return luonne;
    }

    public void setLuonne(String luonne) {
        this.luonne = luonne;
    }

    public String getRooli() {
        return rooli;
    }

    public void setRooli(String rooli) {
        this.rooli = rooli;
    }

    public int getVanhaX() {
        return vanhaX;
    }

    public void setVanhaX(int vanhaX) {
        this.vanhaX = vanhaX;
    }

    public int getVanhaY() {
        return vanhaY;
    }

    public void setVanhaY(int vanhaY) {
        this.vanhaY = vanhaY;
    }

    public boolean isPaikalla() {
        return paikalla;
    }

    public void setPaikalla(boolean paikalla) {
        this.paikalla = paikalla;
        if(!paikalla) this.ruutu = null;
    }

    public Ruutu getRuutu() {
        return ruutu;
    }

    public void setRuutu(Ruutu ruutu) {
        this.ruutu = ruutu;
        if(ruutu == null) this.paikalla = false;
    }

    public String[] getKeskustelut() {
        return keskustelut;
    }

    public void setKeskustelut(String[] keskustelut) {
        this.keskustelut = keskustelut;
    }

    @Override
    public String toString() {
        return "Asiakas{" + "nimi=" + nimi + ", promille=" + promille + ", luonne=" + luonne + ", rooli=" + rooli + ", vanhaX=" + vanhaX + ", vanhaY=" + vanhaY + ", paikalla=" + paikalla + ", ruutu=" + ruutu + ", keskustelut=" + keskustelut + '}';
    }
}
