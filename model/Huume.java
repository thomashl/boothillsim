/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
import controller.Controller;
/**
 *
 * @author lauri
 */
public class Huume {
    private int nimi;
    private int hinta;
    private String vaikutus;

    public Huume(int nimi, int hinta, String vaikutus) {
        this.nimi = nimi;
        this.hinta = hinta;
        this.vaikutus = vaikutus;
    }

    public int getNimi() {
        return nimi;
    }

    public void setNimi(int nimi) {
        this.nimi = nimi;
    }

    public int getHinta() {
        return hinta;
    }

    public void setHinta(int hinta) {
        this.hinta = hinta;
    }

    public String getVaikutus() {
        return vaikutus;
    }

    public void setVaikutus(String vaikutus) {
        this.vaikutus = vaikutus;
    }
}
