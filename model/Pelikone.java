/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Random;

/**
 *
 * @author lauri
}
 */
public class Pelikone {
    public static boolean pelaa(Pelaaja pelaaja, int panos) {
        Random rand = new Random();
        int rn = rand.nextInt(100);
        if(rn < 20){
            pelaaja.setRaha(pelaaja.getRaha()+panos*10);
            return true;
        } else {
            pelaaja.setRaha(pelaaja.getRaha()-panos);
            return false;
        }
    }
}
