/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author lauri
 */
public class Biljardi {    
    public static boolean pelaa(Pelaaja pelaaja) {
        if(pelaaja.getJuoma() != null) pelaaja.juoKaikki(pelaaja.getJuoma());
        double promillet = pelaaja.getPromillet();    //Haetaan pelaajan promillet
        int pelaajaTaso;
        double taso1 = 0.5;
        double taso2 = 1.0;
        double taso3 = 1.5;
        double taso4 = 2.0;
        double taso5 = 2.5;
        double taso6 = 3.0;
        double taso7 = 3.5;
        double taso8 = 4.0;
        
        if (promillet < taso1) {  //Lasketaan pelaajan voitto mahdollisuudet promillejen perusteella
            pelaajaTaso = 50;
        }else if (promillet > taso1 && promillet <= taso2) {
            pelaajaTaso = 75;
        }else if (promillet > taso2 && promillet < taso3) {
            pelaajaTaso = 45;
        }else if (promillet > taso3 && promillet < taso4) {
            pelaajaTaso = 40;
        }else if (promillet > taso4 && promillet < taso5) {
            pelaajaTaso = 35;
        }else if (promillet > taso5 && promillet < taso6) {
            pelaajaTaso = 30;
        }else if (promillet > taso6 && promillet < taso7) {
            pelaajaTaso = 25;
        }else {
            pelaajaTaso = 20;
        }
        
        double vastustajaTaso = (Math.random() + 0.01) * 100;     //"Vastustajan" taso
        
        if (pelaajaTaso >= vastustajaTaso) { //voitto vai häviö
            int nykMaine = pelaaja.getMaine();
            pelaaja.setMaine(nykMaine + 5);
            return true;
        }else {
            return false;
        }
    }
}