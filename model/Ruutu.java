package model;

public class Ruutu {
    private int ruutuX;
    private int ruutuY;
    private String tyyppi;
    private boolean avoin;
    private Asiakas asiakas = null;
    
    public Ruutu(int koordX,int koordY, String tyyppi){
        this.ruutuX = koordX;
        this.ruutuY = koordY;
        this.tyyppi = tyyppi;
        if (tyyppi.matches("lattia|tuoli|tupakkakoppi|tupakka-tuoli|insinööripöytä")){
            this.avoin = true;
        }else{
            this.avoin = false;
        }
    }

    public void setRuutuX(int ruutuX) {
        this.ruutuX = ruutuX;
    }

    public void setRuutuY(int ruutuY) {
        this.ruutuY = ruutuY;
    }

    public void setTyyppi(String tyyppi) {
        this.tyyppi = tyyppi;
    }

    public void setAvoin(boolean avoin) {
        this.avoin = avoin;
    }

    public void setAsiakas(Asiakas asiakas) {
        this.asiakas = asiakas;
    }

    public int getRuutuX() {
        return ruutuX;
    }

    public int getRuutuY() {
        return ruutuY;
    }

    public String getTyyppi() {
        return tyyppi;
    }

    public boolean isAvoin() {
        return avoin;
    }

    public Asiakas getAsiakas() {
        return asiakas;
    }
    
    public String toString() {
        return "" + this.ruutuX + "," + this.ruutuY + " tyyppi: " + this.tyyppi;
    }
}
