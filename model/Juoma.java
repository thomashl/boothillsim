package model;

public class Juoma {
    private String nimi;
    private double alkoholi; // Prosentteina
    private int tilavuus; // Senttilitroina
    private int hinta; // Sentteinä, eli € * 100
    
    public Juoma(String nimi,double alko, int tilavuus, int hinta){
        this.nimi = nimi;
        this.alkoholi = alko;
        this.tilavuus = tilavuus;
        this.hinta = hinta;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    public void setAlkoholi(double alkoholi) {
        this.alkoholi = alkoholi;
    }

    public void setTilavuus(int tilavuus) {
        this.tilavuus = tilavuus;
    }

    public void setHinta(int hinta) {
        this.hinta = hinta;
    }

    public String getNimi() {
        return nimi;
    }

    public double getAlkoholi() {
        return alkoholi;
    }

    public int getTilavuus() {
        return tilavuus;
    }

    public int getHinta() {
        return hinta;
    }
}
