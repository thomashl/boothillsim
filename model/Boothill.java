package model;
import java.util.ArrayList;
public class Boothill {
    private Ruutu[][] kartta; // Huom. viittaus on muotoa kartta[y][x]
    private ArrayList<Asiakas> asiakkaat;
    
    public Boothill(){
        
    }
    public Boothill(Ruutu[][] ruudut, ArrayList<Asiakas> asiakkaat){
        this.kartta = ruudut;
        this.asiakkaat = asiakkaat;
    }
    
    public Ruutu getRuutu(int koordX,int koordY){
        return this.kartta[koordY][koordX];
    }

    public Ruutu[][] getKartta() {
        return kartta;
    }

    public ArrayList<Asiakas> getAsiakkaat() {
        return asiakkaat;
    }

    public void setKartta(Ruutu[][] kartta) {
        this.kartta = kartta;
    }

    public void setAsiakkaat(ArrayList<Asiakas> asiakkaat) {
        this.asiakkaat = asiakkaat;
    }
    
    
}
