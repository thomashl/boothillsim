package model;

import java.util.ArrayList;

public class Pelaaja {
    private String nimi;
    private double promillet;
    private int raha; // Sentteinä, eli €-muoto on /100
    private int pisteet;
    private int hupi;
    private int tupakka;
    private int rakko;
    private int maine;
    private int paino; // kg
    private Ruutu ruutu;
    private Juoma juoma;
    private ArrayList<Juoma> juomahistoria = new ArrayList();
    private ArrayList<String> tappeluhistoria = new ArrayList();
    private final int JUOMA_MÄÄRÄ = 8;
    private final double NESTETILAVUUSKERROIN = 0.7;
    private final double ALKOHOLIN_TIHEYS = 0.79;
    private final double SYÖMÄVAIKUTUS = 0.2; // Syömisen vaikutus promilleihin (vähennetään)
    private final int HUUMEVAIKUTUS = 10; // Huumeiden vaikutus hupiin (lisätään)
    private final int POLTTOJAKAJA = 15; // 1 = yksi sykli polttaa kuin 1 tunti; 60 = kuin 1 minuutti
    
    public void juoHorppy(Juoma juoma){
        // Juodaan juomasta kerralla 4cl
        if(juoma.getTilavuus() <= JUOMA_MÄÄRÄ){
            juo(juoma, juoma.getTilavuus());
            juoma.setTilavuus(0);
            this.setJuoma(null);
            this.juomahistoria.add(juoma);
            return;
        }
        juoma.setTilavuus(juoma.getTilavuus()-JUOMA_MÄÄRÄ);
        juo(juoma, JUOMA_MÄÄRÄ);
    }
    public void juoKaikki(Juoma juoma){
        // Juodaan juoma kokonaan
        juo(juoma, juoma.getTilavuus());
        juoma.setTilavuus(0);
        this.setJuoma(null);
        this.juomahistoria.add(juoma);
    }
    public void juo(Juoma juoma, int määrä){
        double alkoholia = (määrä * 10 * juoma.getAlkoholi() * ALKOHOLIN_TIHEYS ) / 100;
        double promilleja = (alkoholia) / (this.paino * NESTETILAVUUSKERROIN);
        this.setRakko(this.getRakko() + määrä/2);
        this.setPromillet(this.getPromillet() + promilleja);
    }
    public void huumaa(Huume huume){
        this.hupi += HUUMEVAIKUTUS;
    }
    public boolean maksa(int hinta){
        if (hinta > this.raha){
            return false;
        }
        this.raha -= hinta;
        return true;
    }
    public void syo(){
        this.promillet -= SYÖMÄVAIKUTUS;
    }
    public boolean sykli(){
        // return true; jos tuli game over.
        
        // MUUTETAAN HUPIA PROMILLEJEN MUKAAN
        if(this.promillet < 0.5){
            setHupi(this.hupi - 1);
        } else if(this.promillet < 1.0){
            setHupi(this.hupi);
        } else if(this.promillet < 1.5){
            setHupi(this.hupi + 1);
        } else if(this.promillet < 2.0){
            // SWEET SPOT
            setHupi(this.hupi + 5);
        } else if(this.promillet < 2.5){
            setHupi(this.hupi + 1);
        } else if(this.promillet < 3.0){
            setHupi(this.hupi);
        } else if(this.promillet < 3.5){
            setHupi(this.hupi - 1);
        } else {
            setHupi(this.hupi - 2);
        }
        
        // POLTETAAN ALKOHOLIA
        // Muutosidea: kirjataan pelaajan alkoholi grammoissa ja lasketaan promillet antaessa ulos?
        double poltto = paino / 10.0 / POLTTOJAKAJA;
        double a_gram = promillet * paino * NESTETILAVUUSKERROIN;
        this.setPromillet((a_gram - poltto) / (this.paino * NESTETILAVUUSKERROIN));
        
        // NOSTETAAN RAKKOA
        setRakko(this.rakko + 1);
        
        // ANNETAAN PISTEITÄ HUVIN MUKAAN
        int pinnat = 0;
        if(this.hupi > 50) pinnat = this.hupi/10;
        setPisteet(this.pisteet + pinnat );
        
        // TARKISTETAAN HÄVIÖTILAT
        if(this.rakko >= 200) return true;
        if(this.promillet >= 4.0) return true;
        // if(this.promillet <= 0.0) return true; // Kommentit pois jos halutaan että peli loppuu jos on liian selvä
        if(this.hupi <= 0) return true;
        
        return false;
    }

    public Pelaaja() {
    }
    public Pelaaja(String nimi, double promillet, int raha, int pisteet, int hupi, int tupakka, int rakko, int maine, int paino, Juoma juoma) {
        this.nimi = nimi;
        this.promillet = promillet;
        this.raha = raha;
        this.pisteet = pisteet;
        this.hupi = hupi;
        this.tupakka = tupakka;
        this.rakko = rakko;
        this.maine = maine;
        this.paino = paino;
        //this.ruutu = ruutu;
        this.juoma = juoma;
    }
    public String getNimi() {
        return nimi;
    }
    public void setNimi(String nimi) {
        this.nimi = nimi;
    }
    public double getPromillet() {
        return promillet;
    }
    public void setPromillet(double promillet) {
        this.promillet = promillet;
        if(this.promillet < 0.0) this.promillet = 0.0;
    }
    public int getRaha() {
        return raha;
    }
    public void setRaha(int raha) {
        this.raha = raha;
        if(this.raha < 0) this.raha = 0;
    }
    public int getPisteet() {
        return pisteet;
    }
    public void setPisteet(int pisteet) {
        this.pisteet = pisteet;
        if(this.pisteet < 0) this.pisteet = 0;
    }
    public int getHupi() {
        return hupi;
    }
    public void setHupi(int hupi) {
        this.hupi = hupi;
        if(this.hupi > 100) this.hupi = 100;
        if(this.hupi < 0) this.hupi = 0;
    }
    public int getTupakka() {
        return tupakka;
    }
    public void setTupakka(int tupakka) {
        this.tupakka = tupakka;
        if(this.tupakka < 0) this.tupakka = 0;
    }
    public int getRakko() {
        return rakko;
    }
    public void setRakko(int rakko) {
        this.rakko = rakko;
        if(this.rakko > 200) this.rakko = 200;
        if(this.rakko < 0) this.rakko = 0;
    }
    public int getMaine() {
        return maine;
    }
    public void setMaine(int maine) {
        this.maine = maine;
        if(this.maine < 0) this.maine = 0;
    }
    public int getPaino() {
        return paino;
    }
    public void setPaino(int paino) {
        this.paino = paino;
    }
    public Ruutu getRuutu() {
        return ruutu;
    }
    public void setRuutu(Ruutu ruutu) {
        this.ruutu = ruutu;
    }
    public Juoma getJuoma() {
        return juoma;
    }
    public void setJuoma(Juoma juoma) {
        this.juoma = juoma;
    }
    public ArrayList<Juoma> getJuomahistoria() {
        return juomahistoria;
    }
    public void setJuomahistoria(ArrayList<Juoma> juomahistoria) {
        this.juomahistoria = juomahistoria;
    }
    public ArrayList<String> getTappeluhistoria() {
        return tappeluhistoria;
    }
    public void setTappeluhistoria(ArrayList<String> tappeluhistoria) {
        this.tappeluhistoria = tappeluhistoria;
    }

    @Override
    public String toString() {
        String str_juoma = "ei mitään";
        if(juoma != null) str_juoma = juoma.getNimi();
        return "" + nimi + " " + String.format("%.2f", promillet) + "‰ " + raha/100.0 + "€ " + pisteet + " pistettä Hupi: " + hupi + " " + tupakka + " röökiä Rakko: " + rakko + "/200 Maine: " + maine + " " + paino + "kg Juoma: " + str_juoma;
    }
}
